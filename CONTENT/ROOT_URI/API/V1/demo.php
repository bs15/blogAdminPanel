if (isset($_GET['userList'])) {
	$sql = "SELECT * FROM ATHENEUM_STUDENT ORDER BY ID DESC";
	$result = mysqli_query($link,$sql);
	if($result){
		if(mysqli_num_rows($result)>0){
			while($row = mysqli_fetch_array($result,MYSQLI_ASSOC)){

				echo '<tr>
				     <td>'.$row['ID'].'</td>
				     <td><a href="singleUser?id='.$row['UNI_ID'].'">'.$row['NAME'].'</a></td>
				     <td>'.$row['EMAIL'].'</td>
				      <td>'.$row['PHONE'].'</td>
				      <td>'.$row['ENROLL_DATE'].'</td>'
				      ;
				if ($row['PROGRAM_TYPE'] != null && $row['PROGRAM_NAME'] !=null) {
					echo '<td><i class="fas fa-check-circle"></i></td>';
				}else{
					echo '<td><i class="fa fa-times-circle-o"></i></td>';
				}
				echo ' <td><a href="removeUser?id='.$row['UNI_ID'].'" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></a></td>';
					  

			}

		}else{
			echo '<div class="alert alert-warning">No Data</div>';
		}

	}else{
		echo '<div class="alert alert-danger">Error Running the Query</div>';
		echo '<div class="alert alert-danger">' . mysqli_error($link) . '</div>';
	}
}

		
if (isset($_GET['userSearch'])) {
	$search = $_POST['search'];
	$sql = "SELECT * FROM ATHENEUM_STUDENT WHERE NAME LIKE'%".$search."%'";
	
	$result = mysqli_query($link,$sql);
	if($result){
			
		if(mysqli_num_rows($result)>0){
			while($row = mysqli_fetch_array($result,MYSQLI_ASSOC)){
				echo '<tr>
				     <td>'.$row['UNI_ID'].'</td>
				     <td><a href="singleUser?id='.$row['UNI_ID'].'">'.$row['NAME'].'</a></td>
				     <td>'.$row['EMAIL'].'</td>
				      <td>'.$row['PHONE'].'</td>';
				if ($programType != null) {
					echo '<td><i class="fas fa-check-circle"></i></td>';
				}else{
					echo '<td><i class="fa fa-times-circle-o"></i></td>';
				}
			}

		}else{
			echo '<div class="alert alert-warning">No Data</div>';
		}

	}else{
		echo '<div class="alert alert-danger">Error Running the Query</div>';
		echo '<div class="alert alert-danger">' . mysqli_error($link) . '</div>';
	}
}


function httpPost($url,$params)
{
	$postData = '';
	 //create name value pairs seperated by &
	foreach($params as $k => $v) 
	{ 
	  $postData .= $k . '='.$v.'&'; 
	}
	$postData = rtrim($postData, '&');

	$ch = curl_init();  

	curl_setopt($ch,CURLOPT_URL,$url);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
	curl_setopt($ch,CURLOPT_HEADER, false); 
	curl_setopt($ch, CURLOPT_POST, count($postData));
	curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);    

	$output=curl_exec($ch);

	curl_close($ch);
	return $output;

}