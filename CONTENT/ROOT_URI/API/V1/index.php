<?php
$link = new mysqli(MYSQL_HOST,MYSQL_USER,MYSQL_PASS,MYSQL_DB);
$link->set_charset("utf8");
if(mysqli_connect_error()){
    die("ERROR: UNABLE TO CONNECT: ".mysqli_connect_error());
}
require_once("spaces.php");
$space = new SpacesConnect(DO_KEY, DO_SECRET, DO_SPACE, DO_REGION);
	
function startsWith ($string, $startString) 
	{ 
	    $len = strlen($startString); 
	    return (substr($string, 0, $len) === $startString); 
	} 

function sortByOrder($a, $b) {
	return $b['lastModified'] - $a['lastModified'];
}

if ($_SESSION['LoggedIn']) {
	$data = null;
	if (isset($_GET['uploadImages'])) {
		if (isset($_FILES["uploadedImages"])) {
			foreach($_FILES["uploadedImages"]["tmp_name"] as $key=>$tmp_name) {
				// $data .= $tmp_name;
				$file_name=$_FILES["uploadedImages"]["name"][$key];
			    $file_tmp=$_FILES["uploadedImages"]["tmp_name"][$key];
			    $file_name = preg_replace("![^a-z0-9.]+!i", "-", $file_name);
		    	if($space->UploadFile($file_tmp, "public", "publicImages/".$file_name)){
			      $data .= $file_name." uploaded Successfully. ";
			    }else{
			      $data .= "Could not uploaded ".$file_name.". ";
			    }
			}
		}else{
			$data = "Error";
		}
	}

	if (isset($_GET['getUploadedContent'])) {
		$files = $space->ListObjects();
		$fileLists = array();
		foreach ($files as $key => $value) {
		    // echo $value['Key']."<br>";
		    // echo $value['LastModified']."<br>";
		    if (startsWith($value['Key'], "publicImages/")) {
		    	$newArray = array("fileName"=>$value['Key'],"lastModified"=>$value['LastModified']);
		    	array_push($fileLists,$newArray);
		    }
		}

		usort($fileLists, 'sortByOrder');
		$data = $fileLists;
	}
	if (isset($_GET['deleteFile'])) {
		$fileName = $_GET['fileName'];
		if ($space->DeleteObject($fileName)) {
			$data = "Successfully Deleted";
			$errorm = null;
		}else{
			$errorm = "Error";
			$data = null;
		}
	}
$myObj = new stdClass();
$myObj->data = $data;
$myObj->errorm = $errorm;
$myJSON = json_encode($myObj);
echo $myJSON;
}




?>