<?php if($_SESSION['LoggedIn']){ ?>
<div class="hi" id="vapp">
    <div class="container">
        <h1 style="text-align:center;">{{msg}}</h1>
        <div class="row">
        <div class="col-3">
          <select class="form-control" id="exampleFormControlSelect1" v-model="showDomain" @change="filterDomain()">
            <option value="null">Select Domain</option>
            <option value="all">All Records</option>
            <option value="atheneumglobal.education">atheneumglobal.education</option>
            <option value="teenybeanspreschoolcurriculum.com">teenybeanspreschoolcurriculum.com</option>
            <option value="beanstalkedu.com">beanstalkedu.com</option>
            <option value="teachertrainingbangalore.in">teachertrainingbangalore.in</option>
          </select>
          </div>
        </div>
        <hr>
        <div class="alert alert-warning" v-if="showLoader">Loading...</div>
         <div v-if="showDismissibleAlert" class="alert alert-warning" variant="danger" dismissible>
          {{ noticeAlert }}
        </div>
        <!-- <pulse-loader :loading="false" :color="color" :size="size"></pulse-loader> -->
        <div class="card cc" v-for="page in pages" v-bind:key="page.PAGE_ID">
            <div class="card-header">
                <h5><strong>{{page.DOMAIN}}</strong></h5>
            </div>
            <div class="card-body">
                <h5 class="card-title"><strong>{{page.URI}} / {{page.SLUG}}</strong></h5>
                <p class="card-text">{{page.TITLE}}</p>
                <a :href="'/editBlog?id=' + page.PAGE_ID" class="clWhite"><button class="btn btn-primary clWhite">View / Edit Page</button></a>
                <button class="btn btn-danger clWhite" v-on:click="deletePage(page.PAGE_ID)"><i class="fa fa-trash" aria-hidden="true"></i> Delete Page</button>
            </div>
        </div><br>  
    </div>
</div>


<script>
// import PulseLoader from 'vue-spinner/src/PulseLoader.vue'

const vueApp = new Vue({
  el: '#vapp',
  data () {
    return {
      msg: 'Beanstalk Blog Management',
      pages: '',
      apiDomain : 'https://api.teenybeans.in',
      noticeAlert: null,
      showDismissibleAlert : false,
      showDomain:null,
      showLoader:false,
    }
  },
  methods: {
   filterDomain(){
    this.showLoader = true;
    let self = this;
    if(this.showDomain == 'all'){
      this.fetchData();
    }else{
      fetch(this.apiDomain+'/API/cmsBackend/v1/?somePages&domain='+self.showDomain)
        .then(
          function(response) {
            if (response.status !== 200) {
              console.log('Looks like there was a problem. Status Code: ' +
                response.status);
              return;
            }
            // Examine the text in the response
            response.json().then(function(data) {
              if(data){
                self.showLoader = false;
                self.pages = data;
              }else{
                self.showLoader = false;
                self.noticeAlert = "No blog present. Please create one!";
                self.showDismissibleAlert = true;
                self.pages = null;
              }
              
            });
          }
        )
        .catch(function(err) {
          console.log('Fetch Error :-S', err);
        });
    }
   },
   deletePage(id){
     var self = this;
     if(confirm('are you sure?')){
       let formData = new FormData();
        formData.append('pageId', id);
        formData.append('deletePage', "delete");
        fetch(this.apiDomain+"/API/cmsBackend/v1/", {
                method: "POST",
                body:formData,
            }).then(
                function(response) {
                response.json().then(function(data) {
                  self.noticeAlert = data;
                  self.showDismissibleAlert = true;
                  self.fetchData();
                });
              }
            )
            .catch(function(err) {
              console.log('Fetch Error :-S', err);
            });
     }
   },
   fetchData(){
    this.showLoader = true;
     let self = this;
    fetch(this.apiDomain+'/API/cmsBackend/v1/?allPages')
        .then(
          function(response) {
            if (response.status !== 200) {
              console.log('Looks like there was a problem. Status Code: ' +
                response.status);
              return;
            }

            // Examine the text in the response
            response.json().then(function(data) {
              if(data){
                self.pages = data;
                self.showLoader = false;
              }else{
                self.noticeAlert = "No blog present. Please create one!";
                self.showDismissibleAlert = true;
                self.pages = null;
              }
              
            });
          }
        )
        .catch(function(err) {
          console.log('Fetch Error :-S', err);
        });
   }
  },
  created: function(){
    this.fetchData()
  }
})
</script>

<!-- Add "scoped" attribute to limit CSS to this component only -->
<style scoped>
.container{
  text-align: left;
}
h1, h2 {
  font-weight: normal;
}
ul {
  list-style-type: none;
  padding: 0;
}
li {
  display: inline-block;
  margin: 0 10px;
}
a {
  color: #42b983;
}
.cc{
  margin-bottom: 20px;
}
.clWhite{
  color: #ffffff;
  text-decoration:none;
}
</style>

<?php 
}else{
  include 'signIn.php';
}

 ?>