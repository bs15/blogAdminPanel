<script>
  if ( window.history.replaceState ) {
      window.history.replaceState( null, null, window.location.href );
  }
</script>
<?php if($_SESSION['LoggedIn']){ ?>
<div class="container" id="imageUpload">
	<div class="alert alert-warning" v-if="showMessage">{{ responseFromServer }}</div>
  <div class="card border-bottom shadow mb-3 bg-white rounded">
    <div class="card-body">
      <h2>Upload Files to Digital Ocean Spaces</h2>
      <br>
      <form method='POST' class="form" enctype="multipart/form-data">
	      <input type='file' ref="uploadedImages" name='upload_file' multiple/>
	      <input type='submit' :disabled="disableButton" @click="saveData" class="btn btn-success" name="upload_files" value='Upload'/><p v-if="showLoader" style="font-weight: bold;"><i>Loading...</i></p>
      </form>
    </div>
  </div>
  <h2 class="text-center">Uploaded Files</h2>
  <div class="alert alert-warning fixed" v-if="showFetchLoader">Loading...</div>
	 <div v-if="showDismissibleAlert" class="alert alert-warning" variant="danger" dismissible>
	  {{ noticeAlert }}
	</div>
  <div class="card mt-2" v-for="image in fetchedImages">
     <div class="card-body">
        <div class="row">
            <div class="col-md-2 col-sm-12">
                <img class="img-responsive" width="90%" height="100" :src="downloadLinkUrl+image.fileName">
            </div>
            <div class="col-md-10 col-sm-12">
                <h5 class="card-text">Image Link:- {{downloadLinkUrl+image.fileName}}</h5>
                <a :href="downloadLinkUrl+image.fileName" class="btn btn-primary" :download="downloadLinkUrl+image.fileName">Download</a>
                <button class="btn btn-danger" @click="deleteFile(image.fileName)" :disabled="isDisabled">Delete</button>
            </div>
        </div>
     </div>   
  </div> 
</div>

<script>
const vueApp = new Vue({
  el: '#imageUpload',
  data: { 
   display: 'redbox',
   showLoader: false,
   uploadedImages: [],
   fetchedImages: '',
   showMessage:false,
   disableButton:false,
   responseFromServer:null,
   showFetchLoader:false,
   showDismissibleAlert:false,
   noticeAlert:null,
   isDisabled:false,
   downloadLinkUrl: 'https://bscdn.sgp1.digitaloceanspaces.com/',
  },
  methods: {
  	saveData(){
  	  this.showMessage = false;
  	  var self = this;
      this.showLoader = true;
      this.disableButton = true;
      let formData = new FormData();

      if (this.$refs.uploadedImages != null) {
        if (this.$refs.uploadedImages.files.length == 0) {
          this.uploadedImages = null;
        }else{
          for( var i = 0; i < this.$refs.uploadedImages.files.length; i++ ){
            let file = this.$refs.uploadedImages.files[i];
            // console.log(file);
            formData.append('uploadedImages[' + i + ']', file);
          } 
        }
      }

      fetch("/API/V1/?uploadImages", {
          method: "POST",
          body:formData,
      }).then(
          function(response) {
          response.json().then(function(data) {
            // console.log(data);
            self.showMessage = true;
            self.showLoader = false;
            self.disableButton = false;
            self.uploadedImages = null;
            self.responseFromServer = data.data;
            self.fetchUploadedFiles();
            // console.log(data)
            // self.fetchDayContent();
          });
        }
      )
      .catch(function(err) {
        console.log('Fetch Error :-S', err);
      });
  	},
  	fetchUploadedFiles(){
  		this.showFetchLoader = true;
      this.showMessage = false;
  		this.showDismissibleAlert = false;
  		var self = this;
  		fetch('/API/V1/?getUploadedContent')
        .then(function(response) {
          if (response.status !== 200) {
            console.log(
              "Looks like there was a problem. Status Code: " + response.status
            );
            return;
          }
          response.json().then(function(data) {
            // console.log(data);
            self.showFetchLoader = false;
            if (data) {
            	self.fetchedImages = data.data;	
            }else{
            	self.showDismissibleAlert = true;
            	self.noticeAlert="No Images";
            }
          });
        })
        .catch(function(err) {
          console.log("Fetch Error :-S", err);
        });
  	},
    deleteFile(fileName){
      var result = confirm("Want to delete?");
      if (result) {
        this.isDisabled = true;
        this.showFetchLoader = true;
        console.log(fileName);
        var self = this;
        fetch('/API/V1/?deleteFile&fileName='+fileName)
          .then(function(response) {
            if (response.status !== 200) {
              console.log(
                "Looks like there was a problem. Status Code: " + response.status
              );
              return;
            }
            response.json().then(function(data) {
              // console.log(data);
              self.showFetchLoader = false;
              if (data) {
                self.isDisabled = false;
                self.fetchUploadedFiles(); 
              }else{
                self.isDisabled = false;
                self.showDismissibleAlert = true;
                self.noticeAlert="Please Try Again!";
              }
            });
          })
          .catch(function(err) {
            console.log("Fetch Error :-S", err);
          });  
      }
    }
  },
  created(){
  	this.fetchUploadedFiles();
  }
})
</script>


<?php 
}else{
  include 'signIn.php';
}
?>