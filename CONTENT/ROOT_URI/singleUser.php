<script>
      if ( window.history.replaceState ) {
          window.history.replaceState( null, null, window.location.href );
      }
</script>
<script type="text/javascript"> // jquery ajax call
  $(document).ready(function(){  
    $('#confirmPayment').click(function(){
      var userId = $('#userId').html();
      // console.log(userId);
      $.ajax({
          url: "/API/V1/?confirmPayment",
          data: { user: userId },
          dataType:"html",
          type: "post",
          success: function(data){
             console.log(data);
             location.reload(true);
          }
      });
    });
   });
</script>
<script type="text/javascript">
  function sendMail(email, subject, message) {
    let formData = new FormData();
      formData.append('sendMail', 'true');
      formData.append('reciever', email);
      formData.append('sender', 'Admin');
      formData.append('senderMail', 'no-reply@atheneumglobal.com');
      formData.append('subject', subject);
      formData.append('message', message);
      fetch("https://mailapi.teenybeans.in/", {
          method: "POST",
          body:formData,
        }).then(
            function(response) {
            response.json().then(function(data) {
              console.log(data);
            });
          }
        )
        .catch(function(err) {
          console.log('Fetch Error :-S', err);
        });
  }
</script>
<?php 
	$link = mysqli_connect(MYSQL_HOST,MYSQL_USER,MYSQL_PASS,MYSQL_DB);
  $userId = $_GET["id"];
  $email = "";
  $sql = "SELECT * FROM ATHENEUM_STUDENT WHERE UNI_ID = '$userId'";
  $result = mysqli_query($link,$sql);
   if($result){
      if(mysqli_num_rows($result)>0){
        $row = mysqli_fetch_array($result,MYSQLI_ASSOC);
        $userName = $row["NAME"];
        $email = $row["EMAIL"];
        $phone = $row["PHONE"];
        $programType = $row["PROGRAM_TYPE"];
        $programName = $row["PROGRAM_NAME"];
        $status = $row["STATUS"];
        $referals = $row["REFERALS"];
        $referredBy = $row["REFERRED_BY"];
        $earning = $row["EARNING"];
        $paid = $row["PAID"];
        $jsonDetails = $row["DETAILS"];
        $jsonDetails = json_decode($jsonDetails);
        $courseDetails = $row["COURSE_DETAILS"];
        if ($jsonDetails != null) {
          $country = $jsonD->{'country'};
          $state = $jsonD->{'state'};
          $city = $jsonD->{'city'};
          $postal = $jsonD->{'postal'};
          if ($country == "India") {
           $in = true;
           echo "yes";
          }
        }
      }
    }

    // SUBMIT COURSE DETAILS
    if (isset($_POST['submitCourse'])) {
      $courseUser = $_POST['userName'];
      $coursePassword = $_POST['password'];
      $courseDetails = array("url" => "https://learn.atheneumglobal.education", "user" => $courseUser, "password" => $coursePassword);
      $courseDetails =  json_encode($courseDetails);
      $sqlCourse = "UPDATE ATHENEUM_STUDENT SET COURSE_DETAILS = '$courseDetails' WHERE UNI_ID='$userId'";
      $resultCourse = mysqli_query($link, $sqlCourse);
      if ($resultCourse) {
        echo '<div class="container"><div class="alert alert-success">Successfully Submitted</div></div>';
        $sendEmail = $email;
        $subject = "Course Details Access";
        $msg = "<html>
                    <head>
                    <title>Welcome To Athemeum Global College</title>
                    </head>
                    <body style='text-align:center; background:#67E6DC'>
                    <h2>Welcome to Atheneum Global Teacher Traing College </h2>
                    <h3>Hey. You have got the access of your enrolled course.
                    <h3>Please note the password(For course):- ".$coursePassword."</h3>
                     <h3>Go to your Atheneum <a href='https://user.atheneumglobal.education'>Dashboard</a> to see more details.</h3>
                    <h3>Start exploring the course and learning today. </h3>
                    <h3>We have some other exciting courses for you. Please<a href='https://atheneumglobal.education/enroll'>click </a>here and checkout our other courses.</h3>
                    <h3>Hey you can earn money by just referring to your friends or family members. <a href='https://user.athenemglobal.education/refer'>Tell</a> them to join Atheneum Global Teacher Training College now.</h3>
                    <h3>We're in the business of breaking down barriers. Find out how we make practical, career-focused learning more accessible than ever before.</h3>
                    <h3>Thank you.</h3>
                    <h3>- Team Atheneum.</h3>
                    </body>
                  </html>";
        echo '<script>sendMail(`'.$sendEmail.'`, `'.$subject.'`, `'.$msg.'`)</script>';

        $sqlNotification = "INSERT INTO ATHENEUM_NOTIFICATION (`USER_ID`, `USER_TYPE`, `TITLE`, `MESSAGE`, `LINK`, `STATUS`) VALUES ('$userId', 'STUDENT', 'Course Access Given', 'You have been given the course access.', '/', 'PENDING')";
        $resultNotification = mysqli_query($link, $sqlNotification);
        if (!$resultNotification) {
          echo '<div class="container"><div class="alert alert-danger">Some error'.mysqli_error($link).'</div></div>'; 
        }
       } 
    }

 ?>
 <!-- <script type="text/javascript">
  function sendMail(email, subject, message) {
    let formData = new FormData();
      formData.append('sendMail', 'true');
      formData.append('reciever', email);
      formData.append('sender', 'Admin');
      formData.append('senderMail', 'no-reply@atheneumglobal.com');
      formData.append('subject', subject);
      formData.append('message', message);
      fetch("https://mailapi.teenybeans.in/", {
          method: "POST",
          body:formData,
        }).then(
            function(response) {
            response.json().then(function(data) {
              console.log(data);
            });
          }
        )
        .catch(function(err) {
          console.log('Fetch Error :-S', err);
        });
  }
</script> -->

 <?php if (!$_SESSION['LoggedIn']){
 	header("Location: signIn");
 }


 ?>

<?php if($_SESSION['LoggedIn']): ?>

<div class="container">
  <div id="fb-root"></div>
	
	 <div class="row">
        <div class="col-md-12">
          <h1 class="display-7">User Details (<?php echo $userName; ?>)</h1>

          <!-- ---------------USER DETAILS TABLE ------------- -->

          <div class="row">
          <div class="col-12 ml-auto mr-auto">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Basic Details</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
               
                <div class="row">
                  <div class="col">
                    <h5><b>User Id:- </b><p id="userId"><?php echo $userId; ?></p></h5>
                  </div>
                  <div class="col">
                    <h5><b>Email:- </b><?php echo $email; ?></h5>
                  </div>
                  <div class="col">
                    <h5><b>Phone:- </b><?php echo $phone; ?></h5>
                  </div>
                </div>
              </div> 
              <!-- /.card-body -->
            </div>
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">More Details</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="row">
                  <div class="col">
                    <h5><b>Program Type:- </b><?php if ($programType != null) {
                      echo $programType;
                    }else echo "<i>None</i>" ?></h5>
                  </div>
                  <div class="col">
                    <h5><b>Program Name:- </b><?php if ($programName!=null) {
                      echo $programName;
                    }else echo "<i>None</i>" ?></h5>
                  </div>
                  <div class="col">
                    <h5 id="paymentButton"><b>Payment:- </b><?php if($paid == 0){ ?> <button class="btn btn-primary" id="confirmPayment">Confirm Payment</button> <?php } elseif ($paid != null && $courseDetails == null) { ?>
                      <button class="btn btn-warning" id="courseAccess" data-toggle="modal" data-target="#myModal2">Give Course Access</button>
                    <?php }else{
                      if ($in) {
                         echo "Rs. ".$paid;
                      }else{
                        echo "$".$paid;
                      }
                    } ?></h5>
                  </div>
                </div><br>
                <div class="row">
                  <div class="col">
                    <?php 
                    if ($referredBy != null) {
                      $sql2 = "SELECT * FROM ATHENEUM_STUDENT WHERE UNI_ID = '$referredBy'";
                      $result2 = mysqli_query($link, $sql2);
                      $row2 = mysqli_fetch_array($result2, MYSQLI_ASSOC);
                      $referedByName = $row2['NAME']; ?>
                      <h5><b>Referred By:- </b><a href="singleUser?id=<?php echo $referredBy; ?>"><?php echo $referedByName; ?></a></h5>
                    <?php }else{ ?>
                        <h5><b>Referred By:- </b><i>None</i></h5>
                    <?php } ?>
                    
                  </div>
                  <div class="col">
                    <h5><b>References:- </b><button id="references" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Click</button></h5>
                  </div>
                  <div class="col">
                    <h5><b>Mail Status:- </b><?php echo $status; ?></h5>
                  </div>
                </div><br>
                <div class="row">
                  <div class="col">
                    <h5><b>Earning:- </b><?php if ($in) {
                      echo "Rs. ".$earning;
                    }else echo "$".$earning;  ?></h5>
                  </div>
                  <div class="col">
                    <h5><b>Address:- </b>
                      <?php if ($jsonDetails) {
                        echo $country."-".$state. "-". $city. "-" .$postal;
                      }else{
                        echo "None";
                      } ?>
                    </h5>
                  </div>
                  <div class="col"></div>
                </div>
               
              </div> 
              <!-- /.card-body -->
            </div>

            <!-- /.card -->
          </div>
        </div>
          

        </div>
      </div>
</div>
<!-- MODAL FOR REFERALS -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-lg modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          
          <h4 class="modal-title">Referals</h4>
        </div>
        
        <div class="modal-body">
           <div class=" table-responsive p-0">
              <table class="table table-hover text-nowrap" id="franchiseList">
                  <thead>
                    <tr>
                      <th scope="col">Serial No</th>
                      <th scope="col">Email ID's</th>
                      <th scope="col">Registered</th>
                      <th scope="col">Enrolled</th>
                      <th scope="col">Earning</th>
                    </tr>
                  </thead>
                    <tbody>
                    <?php 
                      $sqlReferences = "SELECT * FROM ATHENEUM_STUDENT WHERE UNI_ID = '$userId'";
                      $resultReferences = mysqli_query($link, $sqlReferences);
                      $rowRef = mysqli_fetch_array($resultReferences, MYSQLI_ASSOC);
                      $referals = $rowRef['REFERALS'];
                      if ($referals == null) {
                        echo '<div class="alert alert-warning">No Refarals yet!</div>'; 
                      }else{
                        $referals = explode (",", $referals);
                        $sql = "SELECT * FROM ATHENEUM_STUDENT";
                        $result = mysqli_query($link,$sql);
                        $registeredUser = array();
                        $enrolledUser = array();
                        if(mysqli_num_rows($result)>0){
                          while($row = mysqli_fetch_array($result,MYSQLI_ASSOC)){
                            $userEmail = $row['EMAIL'];
                            $paid = $row['PAID'];
                            if($userEmail != $email){
                              if (in_array($userEmail, $referals)){
                                array_push($registeredUser, $userEmail);
                              }
                              if ($paid != 0) {
                                array_push($enrolledUser, $userEmail);
                              }
                            }
                            
                          }
                        }
                      if ($referals == null) {
                        echo '<div class="alert alert-warning">No reference yet! </div>';
                      }else{
                        
                        foreach ($referals as $key => $value) { ?>
                          <tr>
                            <td scope="col"><?php echo $key+1 ?></td>
                            <td scope="col"><?php echo $value ?></td>
                            <td scope="col"><?php if(in_array($value, $registeredUser)) echo '<i class="fas fa-check-circle"></i>';else echo '<i class="fa fa-times-circle-o" aria-hidden="true"></i>' ?></td>
                            <td scope="col"><?php if(in_array($value, $enrolledUser)) echo '<i class="fas fa-check-circle"></i>';else echo '<i class="fa fa-times-circle-o" aria-hidden="true"></i>' ?></td>
                            <?php 
                              $sql = "SELECT * FROM ATHENEUM_STUDENT WHERE EMAIL = '$value'";
                              $result = mysqli_query($link,$sql);
                              $row = mysqli_fetch_array($result,MYSQLI_ASSOC);
                              if ($in) {
                                if ($row['PROGRAM_TYPE'] == "Certification Course") {
                                $earnedMoney = "Rs. 500";
                                }else if ($row['PROGRAM_TYPE'] == "Graduate Course") {
                                  $earnedMoney = "Rs. 750" ;
                                }else if ($row['PROGRAM_TYPE'] == "Post Graduate Course") {
                                  $earnedMoney = "Rs. 1000";
                                }else{
                                  $earnedMoney = 0;
                                }
                              }else{
                                if ($row['PROGRAM_TYPE'] == "Certification Course") {
                                $earnedMoney = "$10";
                                }else if ($row['PROGRAM_TYPE'] == "Graduate Course") {
                                  $earnedMoney = "$15" ;
                                }else if ($row['PROGRAM_TYPE'] == "Post Graduate Course") {
                                  $earnedMoney = "$20";
                                }else{
                                  $earnedMoney = 0;
                                }
                              }
                             ?>
                            <td scope="col"><?php echo $earnedMoney; ?></td>
                          </tr>
                        <?php }
                        
                      }
                              
                    } ?> 
                            
                    
                  </tbody>
                </table>
            </div>
          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>
  </div>

<!-- MODAL FOR COURSE ACCESS DETAILS -->

<div id="myModal2" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          
          <h4 class="modal-title">Course Details</h4>
        </div>
        
        <div class="modal-body">
           <form method="POST">
             <!-- <div class="input-group mb-3">
                <input type="text" class="form-control" placeholder="Course URL" name="url" required>
                <div class="input-group-append">
                  <div class="input-group-text">
                    <span class="fa fa-link"></span>
                  </div>
                </div>
              </div> -->
              <div class="input-group mb-3">
                <input type="text" class="form-control" placeholder="User Name" name="userName" required>
                <div class="input-group-append">
                  <div class="input-group-text">
                    <span class="fa fa-envelope"></span>
                  </div>
                </div>
              </div>
              <div class="input-group mb-3">
                <input type="text" class="form-control" placeholder="Password" name="password" required>
                <div class="input-group-append">
                  <div class="input-group-text">
                    <span class="fa fa-lock"></span>
                  </div>
                </div>
              </div>
              <input type="submit" name="submitCourse" value="Submit" class="btn btn-success">
           </form>
          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>
  </div>

<?php else: ?>
  <div class="row">
    <div class="col-md-6 col-lg-6 col-sm-12 ml-auto mr-auto">
      <div class="register-logo">
        <h2><b>Atheneum Global Teacher Training College</b></h2>
        <p>Admin Dashboard portal</p>
      </div>
      <div class="card">
        <div class="card-body">
          <div class="ml-auto mr-auto text-center">
            <img src="/IMAGES/logo.jpeg" width="50%" height="50%">
          </div>
          <div class="card-text">Welcome Admin . Please <a href="signIn">Login</a> to continue</div>
         
        </div>
        <!-- /.login-card-body -->
      </div>
    </div>
  </div>

<?php endif; ?>
<script type="text/javascript">
   $(function(){
      $("#upload_pro").on('click', function(e){
          e.preventDefault();
          $("#upload1:hidden").trigger('click');
      });
    });
</script>
