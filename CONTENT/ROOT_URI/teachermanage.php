<?php if($_SESSION['LoggedIn']): ?>

<div class="container" id="vapp">
  <div id="fb-root"></div>
	
	 <div class="row">
        <div class="col-md-12">
          <h1 class="display-7">Manage Teachers</h1>
          <div class="user">
            <p class="lead text-muted">Welcome <?php echo $userName; ?></p>
                <!-- <a href="changePassword" class="btn btn-warning"><i class="fas fa-user"></i>&nbsp;Update Details</a> -->
          </div>
          <br>

          <!-- ---------------USER DETAILS TABLE ------------- -->

          <div class="row">
          <div class="col-12 ml-auto mr-auto">
            <div class="card shadow">
              <div class="card-header">
                <h3 class="card-title">Add a Teacher</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
               <form class="form">
                <div class="text-center">
                  
                  <h5 class="font-weight-bold  card-text btn btn-primary"><img src="/IMAGES/icons/user.svg" height="30" width="30"> Personal Data </h5><hr>
                </div>
                  <div class="row mx-auto">
                    <div class="col-md-6 col-sm-12">
                      <div class="form-group">
                        <label class="font-weight-bold">Teacher Name <span style="color: red">*</span></label>
                        <input type="text" required v-model="teacherName" class="form-control shadow-sm" placeholder="Teacher's Name">
                      </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                      <div class="form-group">
                        <label class="font-weight-bold">Email Id <span style="color: red">*</span></label>
                        <input type="email" required v-model="teacherEmail" class="form-control shadow-sm" placeholder="Teacher's Email Address">
                      </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                      <div class="form-group">
                        <label class="font-weight-bold">Phone Number <span style="color: red">*</span></label>
                        <input type="number" required v-model="teacherPhone" class="form-control shadow-sm" placeholder="Teacher's Phone Number">
                      </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                      <div class="form-group">
                        <label class="font-weight-bold">Qualification <span style="color: red">*</span></label>
                        <input type="text" required v-model="qualification" class="form-control shadow-sm" placeholder="Teacher's Educational Qualification">
                      </div>
                    </div>
                  </div><hr>
                  <div class="text-center">
                    <h5 class="font-weight-bold card-text btn btn-info"><img src="/IMAGES/icons/file.svg" height="30" width="30"> Documents </h5>
                  </div><hr>
                  <div class="row mx-auto">
                    <div class="col-md-6 col-sm-12">
                      <div class="form-group">
                        <label>Upload CV <span style="color: red">*</span></label>
                        <input type="file" ref="teacherCV" class="form-control-file btn btn-outline shadow-sm" name="">
                      </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                      <div class="form-group">
                        <label>Upload Marksheets<span style="color: red">*</span></label>
                        <input type="file" ref="teacherMarksheets" class="form-control-file btn btn-outline shadow-sm" multiple>
                      </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                      <div class="form-group">
                        <label>Upload Certificates<span style="color: red">*</span></label>
                        <input type="file" ref="teacherCertificates" class="form-control-file btn btn-outline shadow-sm" name="" multiple>
                      </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                      <div class="form-group">
                        <label>Upload Profile Picture<span style="color: red">*</span></label>
                        <input type="file" ref="teacherProfilePics" class="form-control-file btn btn-outline shadow-sm" name="">
                      </div>
                    </div>   
                  </div>
                  <hr>
                  <div class="text-center">
                    <h5 class="font-weight-bold  card-text btn btn-warning"><img src="/IMAGES/icons/teacher.svg" height="30" width="30"> Proficiency & Skills </h5>
                  </div>
                  <hr>
                  <div class="row col-md-10 col-sm-12 mx-auto">
                    <div class="col-md-6 col-sm-12">
                      <div class="form-group">
                        <h5><span class="font-weight-bold text-center">Select Department(s)</h5>
                        <div class="custom-control custom-checkbox">
                          <input type="checkbox" class="custom-control-input" id="English" value="English Studies" v-model="selectedDepartments" @click="enableEnglishCheckBox">
                          <label class="custom-control-label" for="English">English Studies</label>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                      <div class="form-group">
                        <h5 class="font-weight-bold">Select Speciality</h5>
                        <div class="custom-control custom-checkbox">
                          <input type="checkbox" class="custom-control-input" id="TEFL" value="TEFL" v-model="selectedEnglish" :disabled="englishSelected">
                          <label class="custom-control-label" for="TEFL">TEFL</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                          <input type="checkbox" class="custom-control-input" id="TESOL" value="TESOL Certification" v-model="selectedEnglish" :disabled="englishSelected">
                          <label class="custom-control-label" for="TESOL">TESOL Certification</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                          <input type="checkbox" class="custom-control-input" id="TESOLM" value="TESOL Masters" v-model="selectedEnglish" :disabled="englishSelected">
                          <label class="custom-control-label" for="TESOLM">TESOL Masters</label>
                        </div>
                      </div>
                    </div>
                  </div>
                  <hr>
                  <div class="row col-md-10 col-sm-12 mx-auto">
                    <div class="col-md-6 col-sm-12">
                      <div class="form-group">
                         <div class="custom-control custom-checkbox">
                          <input type="checkbox" class="custom-control-input" id="Professional" value="Professional Development" v-model="selectedDepartments" @click="enableProfessionalCheckBox">
                          <label class="custom-control-label" for="Professional">Professional Development</label>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                      <div class="form-group">
                        <div class="custom-control custom-checkbox">
                          <input type="checkbox" class="custom-control-input" id="Montessorie" value="Montessorie Teacher Training" v-model="selectedProfessional" :disabled="professionalSelected">
                          <label class="custom-control-label" for="Montessorie">Montessorie Teacher Training</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                          <input type="checkbox" class="custom-control-input" id="PrePrimary" value="Pre-Primary Teacher Training" v-model="selectedProfessional" :disabled="professionalSelected">
                          <label class="custom-control-label" for="PrePrimary">Pre-Primary Teacher Training</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                          <input type="checkbox" class="custom-control-input" id="Nursery" value="Nursery Teacher Training" v-model="selectedProfessional" :disabled="professionalSelected">
                          <label class="custom-control-label" for="Nursery">Nursery Teacher Training</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                          <input type="checkbox" class="custom-control-input" id="Early" value="Early Childhood Education & Care" v-model="selectedProfessional" :disabled="professionalSelected">
                          <label class="custom-control-label" for="Early">Early Childhood Education & Care</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                          <input type="checkbox" class="custom-control-input" id="Education" value="Education Management" v-model="selectedProfessional" :disabled="professionalSelected">
                          <label class="custom-control-label" for="Education">Education Management</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                          <input type="checkbox" class="custom-control-input" id="Child" value="Child Development Associate" v-model="selectedProfessional" :disabled="professionalSelected">
                          <label class="custom-control-label" for="Child">Child Development Associate</label>
                        </div>
                      </div>
                    </div>
                  </div><hr>
                  <div class="row col-md-10 col-sm-12 mx-auto">
                    <div class="col-md-6 col-sm-12">
                      <div class="form-group">
                        <div class="custom-control custom-checkbox">
                          <input type="checkbox" class="custom-control-input" id="Business" value="Business Studies" v-model="selectedDepartments" @click="enableBusinessCheckBox">
                          <label class="custom-control-label" for="Business">Business Studies</label>
                        </div> 
                      </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                      <div class="form-group">
                        <div class="custom-control custom-checkbox">
                          <input type="checkbox" class="custom-control-input" id="level3" value="Ofqual Level 3 Diploma in Business Administration" v-model="selectedBusiness" :disabled="businessSelected">
                          <label class="custom-control-label" for="level3">Ofqual Level 3 Diploma in Business Administration</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                          <input type="checkbox" class="custom-control-input" id="level5" value="Ofqual Level 5 Diploma in Management and Leadership" v-model="selectedBusiness" :disabled="businessSelected">
                          <label class="custom-control-label" for="level5">Ofqual Level 5 Diploma in Management and Leadership</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                          <input type="checkbox" class="custom-control-input" id="Diploma" value="Diploma in Business Administration" v-model="selectedBusiness" :disabled="businessSelected">
                          <label class="custom-control-label" for="Diploma">Diploma in Business Administration</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                          <input type="checkbox" class="custom-control-input" id="EarlyCh" value="Early Childhood Education & Care" v-model="selectedBusiness" :disabled="businessSelected">
                          <label class="custom-control-label" for="EarlyCh">Early Childhood Education & Care</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                          <input type="checkbox" class="custom-control-input" id="Tourism" value="Tourism Management" v-model="selectedBusiness" :disabled="businessSelected">
                          <label class="custom-control-label" for="Tourism">Tourism Management</label>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="text-center mt-2">
                    <div class="row col-md-10 col-sm-12 mx-auto mb-2" v-for="(extra,counter) in extraProf" v-bind:key="counter">
                      <div class="col-md-5 col-sm-12">
                        <input type="text" v-model="extra.dept" required class="form-control shadow-sm" placeholder="Add Another Dept"> 
                      </div>
                      <div class="col-md-5 col-sm-12">
                        <input type="text" required v-model="extra.speciality" class="form-control shadow-sm" placeholder="Add Speciality(Separated By Comma)">
                      </div>
                      <div class="col-md-2 col-sm-12 mx-auto text-center">
                        <button class="btn btn-danger" type="button" @click="deleteProf(counter)"><i class="fa fa-trash" aria-hidden="true"></i></button>
                      </div>
                    </div><br>
                    <button type="button" class="font-weight-bold btn btn-success" @click="addMoreDepartment"><i class="fa fa-plus" aria-hidden="true"></i> Add More Proficiency</button>
                  </div><hr>
                  <div class="text-center">
                    <h5 class="font-weight-bold  card-text btn btn-dark"><img src="/IMAGES/icons/data.svg" height="30" width="30"> More Details </h5>
                  </div><hr>
                  <div class="row col-md-6 mx-auto">
                    <div class="form-group col-12">
                      <label>Give Rating</label>
                      <input type="number" class="form-control" v-model="rating" min="1" max="5" placeholder="Give Rating from 1-5">
                    </div>
                  </div>
                  <div class="col-md-6 col-sm-12 mx-auto">
                    <div class="form-group">
                      <label>About Teacher</label>
                      <textarea v-model="aboutTeacher" cols="4" class="form-control" placeholder="Write About the teacher"></textarea>
                    </div>
                  </div><hr>
                  <div class="alert alert-warning text-center" v-if="warning">{{warningMsg}}</div>
                  <div class="form-group">
                    <button class="btn btn-success float-right" type="button" @click="addTeacher" :disabled="disableButton">Add Teacher</button>
                    <p class="float-right mt-1" v-if="showLoader">Backend API On the way...  </p>
                  </div>
                </form>
              </div>
              <!-- /.card-body -->
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
<?php else: ?>
  <div class="row">
    <div class="col-md-6 col-lg-6 col-sm-12 ml-auto mr-auto">
      <div class="register-logo">
        <h2><b>Atheneum Global Teacher Training College</b></h2>
        <p>Admin Dashboard portal</p>
      </div>
      <div class="card">
        <div class="card-body">
          <div class="ml-auto mr-auto text-center">
            <img src="/IMAGES/logo.jpeg" width="50%" height="50%">
          </div>
          <div class="card-text">Welcome Admin . Please <a href="signIn">Login</a> to continue</div>
         
        </div>
        <!-- /.login-card-body -->
      </div>
    </div>
  </div>

<?php endif; ?>

<script>
const vueApp = new Vue({
  el: '#vapp',
  data: { 
   teacherName:null,
   teacherPhone: null,
   teacherEmail:null,
   qualification:null,
   teacherCV : null,
   teacherMarksheets:null,
   teacherCertificates:null,
   selectedDepartments:[],
   selectedSpecialities:[],
   selectedEnglish:[],
   selectedProfessional:[],
   selectedBusiness:[],
   englishSelected:true,
   businessSelected:true,
   professionalSelected:true,
   responseFromServer:null,
   showMessage:false,
   showLoader:false,
   disableButton:false,
   showLoader:false,
   disableButton:false,
   extraProf:[],
   rating: null,
   aboutTeacher: null,
   warning:false,
   warningMsg:null,
  // extraProfSpec:[
  //      {
  //       specs:''
  //      }
  //   ],
  },
  methods: {
    addMoreDepartment(){
      this.extraProf.push({
        dept:'',
        speciality:[]
      });

      // console.log(this.extraProf);
    },
    deleteProf(counter){
      this.extraProf.splice(counter,1);
    },
    enableEnglishCheckBox(){
      if (this.englishSelected) {
        this.englishSelected = false; 
        this.selectedEnglish = [];
      }else{
        this.englishSelected = true;
        this.selectedEnglish = [];
      }
    },
    enableProfessionalCheckBox(){
      if (this.professionalSelected) {
        this.professionalSelected = false; 
        this.selectedProfessional = [];
      }else{
        this.professionalSelected = true;
        this.selectedProfessional = [];
      }
    },
    enableBusinessCheckBox(){
      if (this.businessSelected) {
        this.businessSelected = false; 
        this.selectedBusiness = [];
      }else{
        this.businessSelected = true;
        this.selectedBusiness = [];
      }
    },
    // fetchDayContent(){
    //   this.showModalContent = false;
    //   this.showModalLoader = true;
    //   var self = this;
    //    fetch('/API/V1/?getContentForTheDay&class='+this.selectedClass+'&day='+this.selectedDay)
    //     .then(function(response) {
    //       if (response.status !== 200) {
    //         console.log(
    //           "Looks like there was a problem. Status Code: " + response.status
    //         );
    //         return;
    //       }
    //       response.json().then(function(data) {
    //         console.log(data);
    //         self.fetchedContentBook = data.contentBook;
    //         self.fetchedWorkBook = data.workBook;
    //         self.fetchedLessonPlan = data.lessonPlan;
    //         self.showModalContent = true;
    //         self.showModalLoader = false;
    //         // console.log(self.fetchedContentBook);
    //       });
    //     })
    //     .catch(function(err) {
    //       console.log("Fetch Error :-S", err);
    //     });
    // },
    addTeacher(){
      var self = this;
      // this.showLoader = true;
      // this.disableButton = true;

      //DEPARTMENTS
      // Get the predefined Departments
      var departments = this.selectedDepartments;

      // Get the User defined departments
      this.extraProf.forEach((item,index)=>{
        departments.push(item.dept);
      });

      // Remove any blank elements
      departments = departments.filter(function (el) {
        return el != "";
      });

      // SPECIALITIES

      //Get Predefined specialities
      var specialities = [];
      if (Array.isArray(this.selectedEnglish) && this.selectedEnglish.length) {
        specialities =  specialities.concat(this.selectedEnglish);
      }
      if (Array.isArray(this.selectedProfessional) && this.selectedProfessional.length) {
        specialities = specialities.concat(this.selectedProfessional); 
      }
      if (Array.isArray(this.selectedBusiness) && this.selectedBusiness.length) {
        specialities = specialities.concat(this.selectedBusiness); 
      }
      var moreSpecialities = [];
      // Get user defined Specialities
      console.log(this.extraProf);
      this.extraProf.forEach((item,index)=>{
        if (item.speciality != null) {
          moreSpecialities = moreSpecialities.concat(item.speciality.split(","));
        }
      });
      if (moreSpecialities.length) {
        specialities = specialities.concat(moreSpecialities);
      }
      console.log(departments);
      // console.log(specialities);


      let formData = new FormData();
      // console.log(this.$refs);

      if (this.teacherName == null || this.teacherEmail == null || this.teacherPhone == null || this.qualification == null) {

      }


      formData.append('class', this.selectedClass);
      formData.append('day', this.selectedDay);

      // fetch("/API/V1/?uploadImages", {
      //     method: "POST",
      //     body:formData,
      // }).then(
      //     function(response) {
      //     response.json().then(function(data) {
      //       console.log(data);
      //       self.showMessage = true;
      //       self.showLoader = false;
      //       self.disableButton = false;
      //       self.responseFromServer = data.result;
      //       self.fetchDayContent();
      //     });
      //   }
      // )
      // .catch(function(err) {
      //   console.log('Fetch Error :-S', err);
      // });

      // console.log(self.contentbookImg); 
    },
  },
})
</script>